const webpack = require('webpack');

module.exports = function override(config, env) {
  
  // Add fallbacks
  config.resolve.fallback = {
    ...config.resolve.fallback,
    assert: require.resolve('assert/'),
    fs: false,
  };

  // Add ProvidePlugin for process and Buffer
  config.plugins = [
    ...config.plugins,
    new webpack.ProvidePlugin({
      process: 'process/browser',
      Buffer: ['buffer', 'Buffer'],
    }),
  ];

  // Modify module rules
  config.module.rules = config.module.rules.map(rule => {
    if (rule.oneOf instanceof Array) {
      rule.oneOf[rule.oneOf.length - 1].exclude = [/\.(js|mjs|jsx|cjs|ts|tsx)$/, /\.html$/, /\.json$/];
    }
    return rule;
  });

  // Ignore source map warnings from node_modules
  config.module.rules.push({
    enforce: 'pre',
    test: /\.js$/,
    loader: 'source-map-loader',
    exclude: /node_modules/,
  });

  // Ignore warnings about failing to parse source maps in node_modules
  config.ignoreWarnings = [{
    module: /node_modules/,
    message: /Failed to parse source map/,
  }];

  return config;
};
