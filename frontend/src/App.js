import './App.css';

import idl from './idl.json';

import {
  Connection,
  PublicKey,
  clusterApiUrl
} from '@solana/web3.js';

import {
  Program,
  AnchorProvider,
  web3,
  utils,
  BN
} from '@project-serum/anchor';

import { useEffect, useState } from 'react';
import { Buffer } from 'buffer';
import { publicKey } from '@project-serum/anchor/dist/cjs/utils';
import { symlinkSync } from 'fs';
window.Buffer = Buffer;

const programID = new PublicKey(idl.metadata.address);
const network = clusterApiUrl("devnet");
const opts = {
  preflightCommitment: "processed"
};
const { SystemProgram } = web3;

const App = () => {
  const [walletAddress, setWalletAddress] = useState(null);
  const [campaigns, setCampaigns] = useState([]);
  const getProvider = async() => {
    const connection = new Connection(network, opts.preflightCommitment);
    const provider = new AnchorProvider(
      connection,
      window.solana,
      opts.preflightCommitment
    );
    return provider;
  };

  const checkIfWalletIsConnected = async() => {
    try {
      const {solana} = window;
      if (solana) {
        if (solana.isPhantom) {
          console.log("Solana wallet found!");
          const response = await solana.connect({
            onlyIfTrusted: true,
          });
          console.log(
            "Connected with Pubkey: ",
            response.publicKey.toString()
          );

          setWalletAddress(response.publicKey.toString());
        }
      } else {
        alert("Get a Phantom Wallet you dingbat");
      }
    } catch(error) {
      console.error(error)
    }
  };

  const connectWallet = async() => {
    try {
      const {solana} = window;
      if (solana) {
        const response = await solana.connect();
        console.log("Wallet Connected")
        setWalletAddress(response.publicKey.toString());
      }
    } catch(error) {
      console.error(error)
    }
  };

  const createCampaign = async() => {
    try {
      const provider = await getProvider();
      const program = new Program(idl, programID, provider);
      const [campaign] = await PublicKey.findProgramAddress(
        [
          utils.bytes.utf8.encode("CAMPAIGN_DEMO"),
          provider.wallet.publicKey.toBuffer(),
        ],
        program.programId
      );
      await program.rpc.create('campaign name', 'campaign description', {
        accounts: {
          campaign,
          user: provider.wallet.publicKey,
          systemProgram: SystemProgram.programId
        }
      });
      console.log("Created a new campaign: ", campaign.toString());
    } catch(error) {
      console.error("Error creating campaign account: ", error);

      // Log detailed error information if available
      if (error.logs) {
        console.error("Transaction logs: ", error.logs);
      }
      if (error.message) {
        console.error("Error message: ", error.message);
      }
      if (error.stack) {
        console.error("Stack trace: ", error.stack);
      }
    }
  };


  const getCampaigns = async() => {
    const connection = new Connection(network, opts.preflightCommitment);
    const provider = await getProvider();
    const program = new Program(idl, programID, provider);
    Promise.all(
      (await connection.getProgramAccounts(programID)).map(
        async (campaign) => ({
          ...(await program.account.campaign.fetch(campaign.pubkey)),
          pubkey: campaign.pubkey,
        })
      )
    ).then(campaigns => setCampaigns(campaigns));
  };

  const withdraw = async publicKeyString => {
    try {
      const provider = await getProvider();
      const program = new Program(idl, programID, provider);

      // Validate and convert the string public key to a PublicKey object
      let campaignPublicKey;
      try {
        campaignPublicKey = new PublicKey(publicKeyString);
      } catch (e) {
        console.error("Invalid PublicKey string:", publicKeyString);
        return;
      }

      const amount = new BN(0.1 * web3.LAMPORTS_PER_SOL);
      const donationSignature = await program.rpc.withdraw(amount, {
        accounts: {
          campaign: campaignPublicKey,
          user: provider.wallet.publicKey,
        },
      });

      console.log("You have withdrew via TX: ", donationSignature, "from ", publicKey.toString());
      getCampaigns();

    } catch(error) {
      console.log("Error: ", error);
    }
  }
  const renderNotConnectedContainer = () => {
    return(<button onClick={connectWallet}>Connect to Wallet</button>);
  };

  const renderConnectedContainer = () => {
    return (
      <>
        <button onClick={createCampaign}>Create Campaign</button>
        <button onClick={getCampaigns}>Get Campaigns</button>
        <br />
        {
          campaigns.map(campaign => (
            <div key={campaign.pubkey.toString()}>
              <p>Campaign ID: {campaign.pubkey.toString()}</p>
              <p>Balance: {(campaign.amountDonated / web3.LAMPORTS_PER_SOL).toString()}</p>
              <p>Campaign Name: {campaign.name}</p>
              <p>Campaign Description: {campaign.description}</p>
              <br />
              <button onClick={() => {donate(campaign.pubkey)}}>Donate</button>
              <button onClick={() => {withdraw(campaign.pubkey)}}>Withdraw</button>
            </div>
          ))
        }
      </>
    );
  };

  const donate = async publicKeyString => {
    const connection = new Connection(network, opts.preflightCommitment);
    try {
      const provider = await getProvider();
      const program = new Program(idl, programID, provider);

      // Validate and convert the string public key to a PublicKey object
      let campaignPublicKey;
      try {
        campaignPublicKey = new PublicKey(publicKeyString);
      } catch (e) {
        console.error("Invalid PublicKey string:", publicKeyString);
        return;
      }

      const amount = new BN(0.1 * web3.LAMPORTS_PER_SOL);
      const donationSignature = await program.rpc.donate(amount, {
        accounts: {
          campaign: campaignPublicKey,
          user: provider.wallet.publicKey,
          systemProgram: SystemProgram.programId,
        },
      });

      console.log("You have donated via TX: ", donationSignature);
      getCampaigns();

    } catch(error) {
      console.error("Error donating: ", error);

      // Log detailed error information if available
      if (error.logs) {
        console.error("Transaction logs: ", error.logs);
      }
      if (error.message) {
        console.error("Error message: ", error.message);
      }
      if (error.stack) {
        console.error("Stack trace: ", error.stack);
      }
    }
  }

  useEffect(() => {
    const onLoad = async() => {
      await checkIfWalletIsConnected();
    };
    window.addEventListener("load", onLoad);
    return () => window.removeEventListener("load", onLoad);
  }, []);

  return ( 
    <div className="App">
      {!walletAddress && renderNotConnectedContainer()}
      {walletAddress && renderConnectedContainer()}
    </div>
  );
};
export default App;
